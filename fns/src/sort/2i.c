// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdint.h>
#include <stdbool.h>

#include "common.h"


const char    ky_name[8] = "sort_2i";
const uint8_t ky_inp     = 2;
const uint8_t ky_out     = 2;

bool
ky_func(uint8_t* out, const uint8_t* inp)
{
  sort(out, inp, 2, cmp_sig);
  return true;
}
