// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#ifndef KY_FNS_SORT_COMMON_H
#define KY_FNS_SORT_COMMON_H

#include <stdint.h>


int cmp_uns(const void* a, const void* b);
int cmp_sig(const void* a, const void* b);

void sort(uint8_t* out, const uint8_t* in, const uint8_t cnt,
  int (*cmp)(const void*, const void*));

#endif
