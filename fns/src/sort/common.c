// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdlib.h>
#include <string.h>

#include "common.h"


/// Compare two 8-bit unsigned integers.
/// @return comparison
/// @retval -1 a is greater
/// @retval 0  a and b are equal
/// @retval 1  b is greater
///
/// @param[in] a first key
/// @param[in] b second key
int
cmp_uns(const void* a, const void* b)
{
  uint8_t x;
  uint8_t y;

  x = *(uint8_t*)a;
  y = *(uint8_t*)b;

  if (x > y)
    return -1;

  if (x == y)
    return 0;
  else
    return 1;
}

/// Compare two 8-bit signed integers.
/// @return comparison
/// @retval -1 a is greater
/// @retval 0  a and b are equal
/// @retval 1  b is greater
///
/// The assumed number format is "two's complement".
///
/// @param[in] a first key 
/// @param[in] b second key 
int
cmp_sig(const void* a, const void* b)
{
  int8_t x;
  int8_t y;

  x = *(int8_t*)a;
  y = *(int8_t*)b;

  if (x > y)
    return -1;

  if (x == y)
    return 0;
  else
    return 1;
}

/// Sort an array of objects.
/// @return input validation
/// @retval 1 all inputs are valid
///
/// @param[out] out output array
/// @param[in]  inp input array
/// @param[in]  cnt number of input array elements
/// @param[in]  cmp comparison function
void
sort(uint8_t* out,
     const uint8_t* in,
     const uint8_t cnt,
     int (*cmp)(const void*, const void*))
{
  (void)memcpy(out, in, cnt);
  (void)qsort(out, cnt, 1, cmp);
}
