// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdint.h>
#include <stdbool.h>

#include "common.h"


const char    ky_name[8] = "sort_3u";
const uint8_t ky_inp     = 3;
const uint8_t ky_out     = 3;

bool
ky_func(uint8_t* out, const uint8_t* inp)
{
  sort(out, inp, 3, cmp_uns);
  return true;
}
