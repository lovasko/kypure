// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdint.h>
#include <stdbool.h>
#include <string.h>


const char    ky_name[9] = "bits_pop";
const uint8_t ky_inp     = 1;
const uint8_t ky_out     = 1;

bool
ky_func(uint8_t* out, const uint8_t* inp)
{
  uint8_t i;
  uint8_t x;

  *out = 0;
  x = *inp;

  for (i = 0; i < 8; i++) {
    if (x & 1)
      (*out)++;
    x >>= 1;
  }
  
  return true;
}
