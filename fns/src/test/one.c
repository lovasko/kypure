// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdint.h>
#include <stdbool.h>
#include <string.h>


const char    ky_name[9] = "test_one";
const uint8_t ky_inp     = 1;
const uint8_t ky_out     = 1;

/// The expected generated program for this function would look like this:
///   add i01  01 -> $00
///   mul $00  02 -> o00
bool
ky_func(uint8_t* out, const uint8_t* inp)
{
  uint8_t var;

  var    = inp[0] + (uint8_t)1;
  out[0] = var    * (uint8_t)2;

  return true;
}
