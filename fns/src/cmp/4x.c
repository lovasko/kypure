// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdbool.h>
#include <stdint.h>


const char    ky_name[7] = "cmp_4x";
const uint8_t ky_inp     = 4;
const uint8_t ky_out     = 1;

bool
ky_func(uint8_t* out, const uint8_t* inp)
{
  uint8_t i;

  *out = inp[0];
  for (i = 1; i < 4; i++)
    if (inp[i] > *out)
      *out = inp[i];

  return true;
}
