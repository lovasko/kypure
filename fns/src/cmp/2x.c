// Copyright (c) 2018 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdbool.h>
#include <stdint.h>


const char    ky_name[7] = "cmp_2x";
const uint8_t ky_inp     = 2;
const uint8_t ky_out     = 1;

bool
ky_func(uint8_t* out, const uint8_t* inp)
{
  uint8_t a;
  uint8_t b;

  a = inp[0];
  b = inp[1];

  *out = a > b ? a : b;
  
  return true;
}
