#  Copyright (c) 2018 Daniel Lovasko
#  All Rights Reserved
#
#  Distributed under the terms of the 2-clause BSD License. The full
#  license is in the file LICENSE, distributed as part of this software.

CC = cc
FTM = -D_BSD_SOURCE -D_XOPEN_SOURCE -D_POSIX_C_SOURCE=200809L -D_DEFAULT_SOURCE
CFLAGS = -fPIC -fno-builtin -std=c99 -Wall -Wextra -Werror -O0 $(FTM) -Isrc/
LDFLAGS = 

all: lib/bits/ctz.so \
     lib/bits/pop.so \
     lib/bits/rev.so \
     lib/cmp/2n.so   \
     lib/cmp/2x.so   \
     lib/cmp/3n.so   \
     lib/cmp/3x.so   \
     lib/cmp/4n.so   \
     lib/cmp/4x.so   \
     lib/sort/2i.so  \
     lib/sort/2u.so  \
     lib/sort/3i.so  \
     lib/sort/3u.so  \
     lib/sort/4i.so  \
     lib/sort/4u.so  \
     lib/test/one.so

# shared object files
lib/bits/ctz.so: obj/bits/ctz.o
	$(CC) -shared obj/bits/ctz.o -o lib/bits/ctz.so

lib/bits/pop.so: obj/bits/pop.o
	$(CC) -shared obj/bits/pop.o -o lib/bits/pop.so

lib/bits/rev.so: obj/bits/rev.o
	$(CC) -shared obj/bits/rev.o -o lib/bits/rev.so

lib/cmp/2n.so: obj/cmp/2n.o
	$(CC) -shared obj/cmp/2n.o -o lib/cmp/2n.so

lib/cmp/2x.so: obj/cmp/2x.o
	$(CC) -shared obj/cmp/2x.o -o lib/cmp/2x.so

lib/cmp/3n.so: obj/cmp/3n.o
	$(CC) -shared obj/cmp/3n.o -o lib/cmp/3n.so

lib/cmp/3x.so: obj/cmp/3x.o
	$(CC) -shared obj/cmp/3x.o -o lib/cmp/3x.so

lib/cmp/4n.so: obj/cmp/4n.o
	$(CC) -shared obj/cmp/4n.o -o lib/cmp/4n.so

lib/cmp/4x.so: obj/cmp/4x.o
	$(CC) -shared obj/cmp/4x.o -o lib/cmp/4x.so

lib/sort/2i.so: obj/sort/2i.o obj/sort/common.o
	$(CC) -shared obj/sort/2i.o obj/sort/common.o -o lib/sort/2i.so

lib/sort/2u.so: obj/sort/2u.o obj/sort/common.o
	$(CC) -shared obj/sort/2u.o obj/sort/common.o -o lib/sort/2u.so

lib/sort/3i.so: obj/sort/3i.o obj/sort/common.o
	$(CC) -shared obj/sort/3i.o obj/sort/common.o -o lib/sort/3i.so

lib/sort/3u.so: obj/sort/3u.o obj/sort/common.o
	$(CC) -shared obj/sort/3u.o obj/sort/common.o -o lib/sort/3u.so

lib/sort/4i.so: obj/sort/4i.o obj/sort/common.o
	$(CC) -shared obj/sort/4i.o obj/sort/common.o -o lib/sort/4i.so

lib/sort/4u.so: obj/sort/4u.o obj/sort/common.o
	$(CC) -shared obj/sort/4u.o obj/sort/common.o -o lib/sort/4u.so

lib/test/one.so: obj/test/one.o
	$(CC) -shared obj/test/one.o -o lib/test/one.so

# object files
obj/bits/ctz.o: src/bits/ctz.c
	$(CC) $(CFLAGS) -c src/bits/ctz.c -o obj/bits/ctz.o

obj/bits/pop.o: src/bits/pop.c
	$(CC) $(CFLAGS) -c src/bits/pop.c -o obj/bits/pop.o

obj/bits/rev.o: src/bits/rev.c
	$(CC) $(CFLAGS) -c src/bits/rev.c -o obj/bits/rev.o

obj/cmp/2n.o: src/cmp/2n.c
	$(CC) $(CFLAGS) -c src/cmp/2n.c   -o obj/cmp/2n.o

obj/cmp/2x.o: src/cmp/2x.c
	$(CC) $(CFLAGS) -c src/cmp/2x.c   -o obj/cmp/2x.o

obj/cmp/3n.o: src/cmp/3n.c
	$(CC) $(CFLAGS) -c src/cmp/3n.c   -o obj/cmp/3n.o

obj/cmp/3x.o: src/cmp/3x.c
	$(CC) $(CFLAGS) -c src/cmp/3x.c   -o obj/cmp/3x.o

obj/cmp/4n.o: src/cmp/4n.c
	$(CC) $(CFLAGS) -c src/cmp/4n.c   -o obj/cmp/4n.o

obj/cmp/4x.o: src/cmp/4x.c
	$(CC) $(CFLAGS) -c src/cmp/4x.c   -o obj/cmp/4x.o

obj/sort/2i.o: src/sort/2i.c
	$(CC) $(CFLAGS) -c src/sort/2i.c -o obj/sort/2i.o

obj/sort/2u.o: src/sort/2u.c
	$(CC) $(CFLAGS) -c src/sort/2u.c -o obj/sort/2u.o

obj/sort/3i.o: src/sort/3i.c
	$(CC) $(CFLAGS) -c src/sort/3i.c -o obj/sort/3i.o

obj/sort/3u.o: src/sort/3u.c
	$(CC) $(CFLAGS) -c src/sort/3u.c -o obj/sort/3u.o

obj/sort/4i.o: src/sort/4i.c
	$(CC) $(CFLAGS) -c src/sort/4i.c -o obj/sort/4i.o

obj/sort/4u.o: src/sort/4u.c
	$(CC) $(CFLAGS) -c src/sort/4u.c -o obj/sort/4u.o

obj/sort/common.o: src/sort/common.c
	$(CC) $(CFLAGS) -c src/sort/common.c -o obj/sort/common.o

obj/test/one.o: src/test/one.c
	$(CC) $(CFLAGS) -c src/test/one.c -o obj/test/one.o

clean:
	rm -rf obj/bits/ctz.o
	rm -rf obj/bits/pop.o
	rm -rf obj/bits/rev.o
	rm -rf obj/cmp/2n.o
	rm -rf obj/cmp/2x.o
	rm -rf obj/cmp/3n.o
	rm -rf obj/cmp/3x.o
	rm -rf obj/cmp/4n.o
	rm -rf obj/cmp/4x.o
	rm -rf obj/sort/2i.o
	rm -rf obj/sort/2u.o
	rm -rf obj/sort/3i.o
	rm -rf obj/sort/3u.o
	rm -rf obj/sort/4i.o
	rm -rf obj/sort/4u.o
	rm -rf obj/sort/common.o
	rm -rf obj/test/one.o
	rm -rf lib/bits/ctz.so
	rm -rf lib/bits/pop.so
	rm -rf lib/bits/rev.so
	rm -rf lib/cmp/2n.so
	rm -rf lib/cmp/2x.so
	rm -rf lib/cmp/3n.so
	rm -rf lib/cmp/3x.so
	rm -rf lib/cmp/4n.so
	rm -rf lib/cmp/4x.so
	rm -rf lib/sort/2i.so
	rm -rf lib/sort/2u.so
	rm -rf lib/sort/3i.so
	rm -rf lib/sort/3u.so
	rm -rf lib/sort/4i.so
	rm -rf lib/sort/4u.so
	rm -rf lib/test/one.so
