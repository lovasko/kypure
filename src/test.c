// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "model.h"
#include "proto.h"
#include "types.h"


/// Compare two unsigned integer arrays based on their C boolean value.
/// @return true if they are equal, false otherwise
///
/// @param[in] a first array
/// @param[in] b first array
/// @param[in] l length of both arrays
static bool
fuzcmp(const uint8_t* restrict a, const uint8_t* restrict b, const uint32_t l)
{
  uint32_t i;

  for (i = 0; i < l; i++)
    if (a[i] == 0 && b[i] != 0)
      return false;

  return true;
}

/// Verify that a program produces the same outputs as the computation
/// function for all possible inputs.
/// @return decision
///
/// @param[in] pr program
/// @param[in] fn function
/// @param[in] cf configuration
bool
prog_test_full(const struct prog* pr,
               const struct func* fn,
               const struct conf* cf)
{
  uint8_t inp[MEM_INP_MAX + 1];
  uint8_t out[MEM_OUT_MAX];
  bool retb;
  struct state st;
  uint16_t i;

  info("verifying the found program for all inputs");

  // Reset all input values.
  for (i = 0; i < cf->cf_inp + 1; i++)
    inp[i] = 0;

  while (true) {
    // Check if we reached the final input.
    if (inp[cf->cf_inp] == 1)
      break;

    // Move to the next input. This function expects the `inp` array to be
    // intentionally greater by one.
    for (i = 0; i < cf->cf_inp; i++) {
      if ((uint16_t)inp[i] == cf->cf_cst) {
        inp[i] = 0;
        inp[i + 1]++;
        continue;
      }

      inp[i]++;
      break;
    }

    // Run the external computation.
    retb = fn->fn_comp(out, inp);
    if (retb == true) {
      // Initialise the computation context and run the program.
      (void)memcpy(st.st_inp, inp, (size_t)cf->cf_inp);
      prog_exec(pr, &st);

      // Compare the output vectors.
      if (cf->cf_fuz == true) {
        if (fuzcmp(out, st.st_out, (uint32_t)cf->cf_out) == true)
          return false;
      } else {
        if (memcmp(out, st.st_out, (size_t)cf->cf_out) != 0)
          return false;
      }
    }
  }

  info("program matches for all inputs");
  prog_show(pr);

  return true;
}

/// Verify that a program produces the same outputs as contained in the sample
/// set, given inputs from the sample set.
/// @return decision
///
/// @param[in] pr program
/// @param[in] fn function
/// @param[in] cf configuration
bool
prog_test_samp(const struct prog* pr,
               const struct func* fn,
               const struct conf* cf)
{
  uint16_t i;
  struct state st;

  // Traverse all collected function samples.
  for (i = 0; i < cf->cf_smp; i++) {
    // Initialise the state and run the program.
    (void)memcpy(st.st_inp, fn->fn_sinp[i], (size_t)cf->cf_inp);
    prog_exec(pr, &st);

    // Compare the output vectors.
    if (cf->cf_fuz == true) {
      if (fuzcmp(fn->fn_sout[i], st.st_out, (uint32_t)cf->cf_out) == false)
        return false;
    } else {
      if (memcmp(fn->fn_sout[i], st.st_out, (size_t)cf->cf_out) != 0)
        return false;
    }
  }

  info("program matches for all samples");
  prog_show(pr);

  return true;
}
