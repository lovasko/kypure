// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <inttypes.h>
#include <errno.h>

#include "model.h"
#include "proto.h"
#include "types.h"


// Default values.
#define DEF_FUZ false                   ///< No fuzzy matching.
#define DEF_SMP 16                      ///< Sixteen samples per function.
#define DEF_INP 2                       ///< Single input byte.
#define DEF_OUT 2                       ///< Single output byte.
#define DEF_VAR 4                       ///< Four helper variables.
#define DEF_CST 3                       ///< Use constants 0, 1, 2, and 3.
#define DEF_LEN 8                       ///< 8 instructions per program.
#define DEF_MOS ((uint16_t)1 << MO_NND) ///< Only use NAND operation.

/// Print the help text to the standard output stream.
static void
print_usage(void)
{
  printf(
    "About:\n"
    "  ky - kolmogorov yield\n\n"

    "Usage:\n"
    "  ky [OPTIONS] lib... \n\n"
    
    "Arguments:\n"
    "  lib     Shared library object containing expected symbols.\n\n"

    "Options:\n"
    "  -c CST  Upper limit on numerical constants to use. (def = %d)\n"
    "  -f      Turn on the fuzzy mode of comparison.\n"
    "  -h      Print this help text.\n"
    "  -i INP  Input vector size. (def = %d)\n"
    "  -l LEN  Length of the generated programs. (def = %d)\n"
    "  -m MOP  Selected mathematical operations to use. (def = nnd)\n"
    "  -o OUT  Output vector size. (def = %d)\n"
    "  -s SMP  Number of generated function samples. (def = %d)\n"
    "  -v VAR  Number of helper variables to use. (def = %d)\n",
    DEF_CST, DEF_INP, DEF_LEN, DEF_OUT, DEF_SMP, DEF_VAR
  );
}

/// Convert a string into a bounded unsigned 16-bit integer.
/// @return success/failure indication
///
/// @param[out] i   unsigned 16-bit integer
/// @param[in]  in  input string to parse
/// @param[in]  min lower bound (inclusive)
/// @param[in]  max upper bound (inclusive)
static bool
parse_uint16(uint16_t* i,
             const char* in,
             const intmax_t min,
             const intmax_t max)
{
  intmax_t x;

  errno = 0;
  x = strtoimax(in, NULL, 10);
  if (x == 0 && errno != 0) {
    info("unable to parse number '%s'", in);
    return false;
  }

  if (x < min || x > max) {
    info("number %" PRIiMAX " out of range (%" PRIiMAX "..%" PRIiMAX ")\n",
      x, min, max);
    return false;
  }

  *i = (uint16_t)x;
  return true;
}

/// Parse a human-recognised name of a mathematical operation.
/// @return failure/success indication
///
/// @param[out] mo bit-flags of mathematical operations
/// @param[in]  in string containing comma-separated operation names
static bool
parse_math_op(uint16_t* mo, const char* in)
{
       if (strcmp(in, "add") == 0) *mo |= ((uint16_t)1 << MO_ADD);
  else if (strcmp(in, "mul") == 0) *mo |= ((uint16_t)1 << MO_MUL);
  else if (strcmp(in, "and") == 0) *mo |= ((uint16_t)1 << MO_AND);
  else if (strcmp(in, "or")  == 0) *mo |= ((uint16_t)1 << MO_OR);
  else if (strcmp(in, "xor") == 0) *mo |= ((uint16_t)1 << MO_XOR);
  else if (strcmp(in, "rot") == 0) *mo |= ((uint16_t)1 << MO_ROT);
  else if (strcmp(in, "nnd") == 0) *mo |= ((uint16_t)1 << MO_NND);
  else if (strcmp(in, "nor") == 0) *mo |= ((uint16_t)1 << MO_NOR);
  else {
    info("unknown mathematical operation '%s'", in);
    return false;
  }

  return true;
}

/// Set the number of allowed constants.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input
static bool
option_c(struct conf* cf, const char* in)
{
  return parse_uint16(&cf->cf_cst, in, 1, 255);
}

/// Select the fuzzy matching logic.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input (unused)
static bool
option_f(struct conf* cf, const char* in)
{
  (void)in;
  cf->cf_fuz = true;

  return true;
}

/// Print the help usage.
/// @return success/failure indication
///
/// @param[out] cf configuration (unused)
/// @param[in]  in argument input (unused)
static bool
option_h(struct conf* cf, const char* in)
{
  (void)cf;
  (void)in;

  print_usage();
  exit(EXIT_FAILURE);

  return true;
}

/// Set the size of the input vector.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input
static bool
option_i(struct conf* cf, const char* in)
{
  return parse_uint16(&cf->cf_inp, in, 1, 16);
}

/// Set the size of the output vector.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input
static bool
option_o(struct conf* cf, const char* in)
{
  return parse_uint16(&cf->cf_out, in, 1, 16);
}

/// Set the number of helper variables.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input
static bool
option_v(struct conf* cf, const char* in)
{
  return parse_uint16(&cf->cf_var, in, 0, 16);
}

/// Select a mathematical operation.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input
static bool
option_m(struct conf* cf, const char* in)
{
  return parse_math_op(&cf->cf_mos, in);
}

/// Set the expected program length.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input
static bool
option_l(struct conf* cf, const char* in)
{
  return parse_uint16(&cf->cf_len, in, 1, 64);
}

/// Set the number of function samples.
/// @return success/failure indication
///
/// @param[out] cf configuration
/// @param[in]  in argument input
static bool
option_s(struct conf* cf, const char* in)
{
  return parse_uint16(&cf->cf_smp, in, 1, UINT16_MAX);
}

/// Assign default values to optional input variables.
///
/// @param[in] opts command-line options
static void
set_default(struct conf* cf)
{
  uint16_t i;

  cf->cf_fuz = DEF_FUZ;
  cf->cf_smp = DEF_SMP;
  cf->cf_cst = DEF_CST;
  cf->cf_inp = DEF_INP;
  cf->cf_out = DEF_OUT;
  cf->cf_var = DEF_VAR;
  cf->cf_len = DEF_LEN;

  // DEF_MOS is applied only if this stays zero.
  cf->cf_mos = 0;

  // Assign all objects paths to be non-existent.
  for (i = 0; i < FNS_CNT_MAX; i++)
    cf->cf_path[i] = NULL;
}

/// Generate the getopt(3) DSL based on a set of option definitions.
///
/// @param[out] str DSL string
/// @param[in]  op  array of option definitions
/// @param[in]  nop number of option definitions
static void
getopt_str(char* str,
           const struct option* op,
           const uint64_t nop)
{
  uint64_t sidx; // Index in the generated string.
  uint64_t oidx; // Index in the options array.

  sidx = 0;
  for (oidx = 0; oidx < nop; oidx++) {
    // Append the option name.
    str[sidx] = op[oidx].op_name;
    sidx++;

    // Append a colon if the option expects an argument.
    if (op[oidx].op_arg == true) {
      str[sidx] = ':';
      sidx++;
    }
  }
}

/// Parse the command-line options into a configuration.
/// @return success/failure indication
///
/// @param[out] cf   configuration
/// @param[in]  argc argument count (as seen in the main function)
/// @param[in]  argv argument vector (as seen in the main function)
bool
conf_load(struct conf* cf, int argc, char* argv[])
{
  int opt;
  bool retb;
  uint64_t i;
  int k;
  char optdsl[128];
  struct option op[9] = {
    { 'c',  true,  option_c },
    { 'f',  false, option_f },
    { 'h',  false, option_h },
    { 'l',  true,  option_l },
    { 'm',  true,  option_m },
    { 'i',  true,  option_i },
    { 'o',  true,  option_o },
    { 's',  true,  option_s },
    { 'v',  true,  option_v }
  };

  (void)memset(optdsl, '\0', sizeof(optdsl));
  getopt_str(optdsl, op, 9);

  // Set optional arguments to sensible defaults.
  set_default(cf);

  // Loop through available options.
  while (true) {
    // Parse the next option.
    opt = getopt(argc, argv, optdsl);
    if (opt == -1)
      break;

    // Unknown option.
    if (opt == '?') {
      print_usage();
      info("unknown option '%c'", optopt);

      return false;
    }

    // Find the relevant option.
    for (i = 0; i < 9; i++) {
      if (op[i].op_name == (char)opt) {
        retb = op[i].op_act(cf, optarg);
        if (retb == false) {
          info("action for option '%c' failed", opt);
          return false;
        }

        break;
      }
    }
  }

  // Assign all other arguments as library paths.
  for (k = optind; k < argc; k++)
    cf->cf_path[k - optind] = argv[k];

  // Assign the default mathematical operations if none were selected.
  if (cf->cf_mos == 0)
    cf->cf_mos = DEF_MOS;

  return true;
}
