// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#ifndef KY_PROTO_H
#define KY_PROTO_H

#include <stdint.h>
#include <stdbool.h>

#include "types.h"


// Configuration.
bool conf_load(struct conf* cf, int argc, char* argv[]);
void conf_show(const struct conf* cf);

// Function.
bool func_load(struct func* fn, const uint16_t n, const struct conf* cf);
bool func_samp(struct func* fn, const struct conf* cf);
void func_show(const struct func* func, const struct conf* cf);

// Instruction.
bool inst_iset(const struct inst in, const struct conf* cf);
void inst_show(const struct inst in);
bool inst_next(
  const struct inst in,
  const struct prog* pr,
  const struct conf* cf);

// Program.
bool prog_test_full(
  const struct prog* pr,
  const struct func* fn,
  const struct conf* cf);
bool prog_test_samp(
  const struct prog* pr,
  const struct func* fn,
  const struct conf* cf);
void prog_exec(const struct prog* pr, struct state* st);
void prog_reset(struct prog* pr);
bool prog_next(struct prog* pr, const struct conf* cf);
void prog_show(const struct prog* pr);
bool prog_iset(struct prog* pr, const struct conf* cf);

// Utility.
void info(const char* sf, ...);

#endif
