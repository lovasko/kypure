// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#ifndef KY_MODEL_H
#define KY_MODEL_H

// Mathematical operations.
#define MO_ADD 0 // Arithmetic addition.
#define MO_MUL 1 // Arithmetic multiplication.
#define MO_AND 2 // Bitwise AND.
#define MO_OR  3 // Bitwise OR.
#define MO_XOR 4 // Bitwise XOR.
#define MO_ROT 5 // Bitwise left rotation.
#define MO_NND 6 // Bitwise NAND.
#define MO_NOR 7 // Bitwise NOR.

// Memory types. Note that the order of memory types is important, as the 
// result of an instruction can be stored only in a variable or in the output
// vector and therefore only one bit is sufficient. This fits well with these
// two memory types having numbers 0 and 1.
#define MEM_VAR 0 // Helper variable.
#define MEM_OUT 1 // Output vector element.
#define MEM_INP 2 // Input vector element.
#define MEM_CST 3 // Number constant.

// Virtual machine limits.
#define MEM_VAR_MAX 8  // Helper variables.
#define MEM_OUT_MAX 8  // Output vector.
#define MEM_INP_MAX 8  // Input vector.
#define PRG_LEN_MAX 63 // Maximal length of generated programs.

// Configuration limits.
#define FNS_CNT_MAX 16 // Maximal number of functions to load.
#define SMP_CNT_MAX 32 // Maximal number of samples per function.

#endif
