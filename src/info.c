// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#include "proto.h"


/// Print logging information to the standard output stream.
///
/// @param[in] sf  string format
/// @param[in] ... variable-length arguments for the message
void
info(const char* sf, ...)
{
  char tstr[32];
  struct tm* tfmt;
  time_t traw;
  va_list args;

  // Print the current time in GMT.
  time(&traw);
  tfmt = gmtime(&traw);
  strftime(tstr, sizeof(tstr), "%F %T", tfmt);
  printf("[%s] ", tstr);

  // Print the passed message.
  va_start(args, sf);
  vprintf(sf, args);
  va_end(args);
  printf("\n");
}
