// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdlib.h>
#include <string.h>

#include "types.h"
#include "proto.h"


/// Generate the instruction set for the program.
/// @return success/failure indication
///
/// @param[in] pr program
/// @param[in] cf configuration
bool
prog_iset(struct prog* pr, const struct conf* cf)
{
  struct inst in;
  uint32_t i;
  uint32_t k;

  // Count the number of valid instructions.  
  pr->pr_niset = 0;
  for (i = 0; i < 16777216; i++) {
    (void)memcpy(&in, &i, sizeof(i));
    if (inst_iset(in, cf) == true)
      pr->pr_niset++;
  }

  // Verify that at least one instruction matches.
  if (pr->pr_niset == 0) {
    info("no instructions match the configuration");
    return false;
  }

  // Allocate the necessary memory for the instruction set.
  pr->pr_iset = calloc(pr->pr_niset, sizeof(struct inst));
  if (pr->pr_iset == NULL) {
    info("unable to allocate memory for instruction set");
    return false;
  }

  // Fill the instruction set array.
  k = 0;
  for (i = 0; i < 16777216; i++) {
    (void)memcpy(&in, &i, sizeof(i));
    if (inst_iset(in, cf) == true) {
      pr->pr_iset[k] = in;
      k++;
    }
  }

  return true;
}

/// Reset a program.
///
/// @param[in] pr program
void
prog_reset(struct prog* pr)
{
  uint8_t i;

  for (i = 0; i < PRG_LEN_MAX; i++)
    pr->pr_idx[i] = 0;

  pr->pr_len = 0;
}

/// Advance to the next possible program.
/// @return availability of the next program
/// @retval true  program is available
/// @retval false no more available programs
///
/// @param[in] pr program
/// @param[in] cf configuration
bool
prog_next(struct prog* pr, const struct conf* cf)
{
  bool retb;

  while (true) {
    // Continue until all instructions have been attempted to be added to the
    // program.
    while (pr->pr_idx[pr->pr_len] < pr->pr_niset - 1) {
      // Check whether the instruction constitutes a plausible program.
      retb = inst_next(pr->pr_iset[pr->pr_idx[pr->pr_len]], pr, cf);
      if (retb == true && pr->pr_len < cf->cf_len) {
        pr->pr_len++;

        // Check whether we have a program of sufficient length.
        if (pr->pr_len == cf->cf_len)
          return true;

        continue;
      }

      // Move to the next instruction.
      pr->pr_idx[pr->pr_len]++;
    }

    // In case no instruction was found.
    if (pr->pr_idx[pr->pr_len] >= (pr->pr_niset - 1)) {
      // Check whether the first instruction reached the final option.
      if ((pr->pr_len == 0) && (pr->pr_idx[0] == (pr->pr_niset - 1)))
        return false;

      // Revert to the first instruction on this level.
      pr->pr_idx[pr->pr_len] = 0;

      // Decrease the current level.
      pr->pr_len--;

      // Move to the next instruction in the new level, as the current one has
      // been explored fully.
      pr->pr_idx[pr->pr_len]++;
      continue;
    }
  }

  return true;
}
