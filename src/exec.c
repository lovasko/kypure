// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "model.h"
#include "types.h"
#include "proto.h"


/// Execute a mathematical operation with two arguments.
/// @return result of the mathematical operation
///
/// @param[in] mo mathematical operation
/// @param[in] x  first argument
/// @param[in] y  second argument
static uint8_t
math_op_exec(const uint8_t mo, const uint8_t x, const uint8_t y)
{
  if (mo == MO_ADD) return x + y;
  if (mo == MO_MUL) return x * y;
  if (mo == MO_AND) return x & y;
  if (mo == MO_OR)  return x | y;
  if (mo == MO_XOR) return x ^ y;
  if (mo == MO_ROT) return (uint8_t)(x << y) | (x >> (8 - y));
  if (mo == MO_NND) return ~(x & y);
  if (mo == MO_NOR) return ~(x | y);

  info("unknown mathematical operation: %" PRIu8, mo);
  exit(EXIT_FAILURE);

  // Should not be reached.
  return 0;
}

/// Get the value of an argument.
/// @return argument value
///
/// @param[in] st  state 
/// @param[in] mem memory type
/// @param[in] idx memory location
static uint8_t
arg_val(const struct state* st, const uint8_t mem, const uint8_t idx)
{
  if (mem == MEM_OUT) return st->st_out[idx];
  if (mem == MEM_INP) return st->st_inp[idx];
  if (mem == MEM_VAR) return st->st_var[idx];
  if (mem == MEM_CST) return idx;

  info("unknown memory type: %" PRIu8, mem);
  exit(EXIT_FAILURE);

  // Should not be reached.
  return 0;
}

/// Execute an instruction within a given state.
///
/// @param[in] in instruction
/// @param[in] st state
static void
inst_exec(const struct inst in, struct state* st)
{
  uint8_t* dst;
  uint8_t arg1;
  uint8_t arg2;

  // Obtain the argument values.
  arg1 = arg_val(st, in.in_op1_mem, in.in_op1_val);
  arg2 = arg_val(st, in.in_op2_mem, in.in_op2_val);

  // Calculate the destination address.
  dst = NULL;
  if (in.in_res_mem == MEM_OUT) dst = st->st_out;
  if (in.in_res_mem == MEM_VAR) dst = st->st_var;

  // Assign the new value.
  dst[in.in_res_val] = math_op_exec(in.in_mo, arg1, arg2);
}

/// Execute a program.
///
/// This function expects the state to have it's input vector already filled
/// in. It clears the helper variables and output vector. Once the execution of
/// the program is finished, the output vector is stored within the context.
///
/// @param[in] pr program
/// @param[in] st state
void
prog_exec(const struct prog* pr, struct state* st)
{
  uint8_t i;

  // Clear the mutable state.
  (void)memset(st->st_var, 0, MEM_VAR_MAX);
  (void)memset(st->st_out, 0, MEM_OUT_MAX);

  // Execute all of program's instructions.
  for (i = 0; i < pr->pr_len; i++)
    inst_exec(pr->pr_iset[pr->pr_idx[i]], st);
}
