// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>

#include "types.h"
#include "model.h"
#include "proto.h"


/// Ensure that the output vector memory is written as the last set of
/// instructions. Any computation that would happen afterwards would not
/// materialise into the output vector and therefore would be a waste.
/// @return decision
///
/// @param[in] in proposed instruction
/// @param[in] pr program
/// @param[in] cf configuration
static bool
out_end(const struct inst in, const struct prog* pr, const struct conf* cf)
{
  if (in.in_res_mem != MEM_OUT) 
    return (cf->cf_len - pr->pr_len > cf->cf_out);
  else
    return (cf->cf_len - pr->pr_len <= cf->cf_out);
}

/// Ensure that each output vector element is written into at most once.
/// @return decision
///
/// @param[in] in proposed instruction
/// @param[in] pr program
/// @param[in] cf configuration
static bool
out_once(const struct inst in, const struct prog* pr, const struct conf* cf)
{
  uint16_t used;
  uint8_t i;
  struct inst tmp;

  (void)cf;

  // Early successful exit if we are not examining an instruction that writes
  // to the output vector.
  if (in.in_res_mem != MEM_OUT)
    return true;

  // Compute a bit array of which output vector indices are already used.
  used = 0;
  for (i = 0; i < pr->pr_len; i++) {
    tmp = pr->pr_iset[pr->pr_idx[i]];
    if (tmp.in_res_mem == MEM_OUT)
      used |= (uint16_t)1 << tmp.in_res_val;
  }

  return ((used & ((uint16_t)1 << in.in_res_val)) == 0);
}

/// Ensure that any new variable introduced to the computation is great by
/// at most one index, therefore preventing homomorphisms on variables.
/// @return decision
///
/// @param[in] in proposed instruction
/// @param[in] pr program
/// @param[in] cf configuration
static bool
new_var(const struct inst in, const struct prog* pr, const struct conf* cf)
{
  uint8_t max;
  uint8_t i;
  struct inst tmp;

  (void)cf;

  // Early successful exit if we are not examining an instruction that stores
  // its result in a helper variable.
  if (in.in_res_mem != MEM_VAR)
    return true;

  max = 0;
  for (i = 0; i < pr->pr_len; i++) {
    tmp = pr->pr_iset[pr->pr_idx[i]];
    if (tmp.in_res_mem == MEM_VAR && tmp.in_res_val > max) {
      max = tmp.in_res_val;
    }
  }

  return (in.in_res_val <= max);
}

/// Ensure that each element from the input vector is read only once.
/// @return decision
///
/// @param[in] in proposed instruction
/// @param[in] pr program
/// @param[in] cf configuration
static bool
inp_once(const struct inst in, const struct prog* pr, const struct conf* cf)
{
  uint16_t used;
  uint8_t i;
  struct inst tmp;

  (void)cf;

  // Early exit if the instruction isn't using the input vector.
  if (in.in_op1_mem != MEM_INP || in.in_op2_mem != MEM_INP)
    return true;

  // Compute the already used input vector elements.
  used = 0;
  for (i = 0; i < pr->pr_len; i++) {
    tmp = pr->pr_iset[pr->pr_idx[i]];

    // Add the first operand.
    if (tmp.in_op1_mem == MEM_INP)
      used |= ((uint16_t)1 << tmp.in_op1_val);

    // Add the second operand.
    if (tmp.in_op2_mem == MEM_INP)
      used |= ((uint16_t)1 << tmp.in_op2_val);
  }

  // Test whether the first operand has been used.
  if (in.in_op1_mem == MEM_INP && (used & ((uint16_t)1 << in.in_op1_val)))
    return false;

  // Test whether the second operand has been used.
  if (in.in_op2_mem == MEM_INP && (used & ((uint16_t)1 << in.in_op2_val)))
    return false;

  return true;
}

/// Check whether a particular memory location is written.
/// @return decision
///
/// @param[in] loc  memory location type
/// @param[in] mem  memory location
/// @param[in] val  memory index
/// @param[in] pres presence bit array
static bool
check_written(const uint8_t loc,
              const uint8_t mem,
              const uint8_t val,
              const uint16_t pres)
{
  if (mem == loc)
    if ((pres & ((uint16_t)1 << val)) == 0)
      return false;

  return true;
}

/// Ensure that the new instruction does not use memory locations that were not
/// written into previously, and would not contain a deterministic value.
/// @return decision
///
/// @param[in] in proposed instruction
/// @param[in] pr program
/// @param[in] cf configuration
static bool
unwritten(const struct inst in, const struct prog* pr, const struct conf* cf)
{
  uint16_t wr_var;
  uint16_t wr_out;
  uint8_t i;
  struct inst tmp;

  (void)cf;

  // At the start of the program no elements of the output vector and no helper
  // variables are used.
  wr_var = 0;
  wr_out = 0;

  // Traverse the program and store all written memory locations.
  for (i = 0; i < pr->pr_len; i++) {
    tmp = pr->pr_iset[pr->pr_idx[i]];
    if (tmp.in_res_mem == MEM_VAR) wr_var |= (uint16_t)1 << tmp.in_res_val;
    if (tmp.in_res_mem == MEM_OUT) wr_out |= (uint16_t)1 << tmp.in_res_val;
  }

  // Check whether the first argument is using valid memory.
  return check_written(MEM_VAR, in.in_op1_mem, in.in_op1_val, wr_var)
      && check_written(MEM_VAR, in.in_op2_mem, in.in_op2_val, wr_var)
      && check_written(MEM_OUT, in.in_op1_mem, in.in_op1_val, wr_out)
      && check_written(MEM_OUT, in.in_op2_mem, in.in_op2_val, wr_out);
} 

/// Eliminate further writing to memory that was written but not used since,
/// rendering the previous computation pointless. 
/// @return decision
///
/// @param[in] in proposed instruction
/// @param[in] pr program
/// @param[in] cf configuration
static bool
unused(const struct inst in, const struct prog* pr, const struct conf* cf)
{
  uint16_t wr_var;
  uint16_t wr_out;
  uint8_t i;
  struct inst tmp;

  (void)in;
  (void)cf;

  // At the start of the program no elements of the output vector and no helper
  // variables are used.
  wr_var = 0;
  wr_out = 0;

  for (i = 0; i < pr->pr_len; i++) {
    tmp = pr->pr_iset[pr->pr_idx[i]];

    // 
    if (tmp.in_res_mem == MEM_VAR) wr_var |= (uint16_t)1 << tmp.in_res_val;
    if (tmp.in_res_mem == MEM_OUT) wr_out |= (uint16_t)1 << tmp.in_res_val;
  }

  return true;
}

/// Determine whether an instruction can be added to a program.
/// @return decision
///
/// @param[in] in instruction
/// @param[in] pr program
/// @param[in] cf configuration
bool
inst_next(const struct inst in, const struct prog* pr, const struct conf* cf)
{
  return out_end(in, pr, cf)
      && out_once(in, pr, cf)
      && new_var(in, pr, cf)
      && unwritten(in, pr, cf)
      && inp_once(in, pr, cf)
      && unused(in, pr, cf);
}
