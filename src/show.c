// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>

#include "model.h"
#include "proto.h"
#include "types.h"


/// Mathematical operations.
static const char* mo_str[8] = {
  "add",
  "mul",
  "and",
  "or",
  "xor",
  "rot",
  "nnd",
  "nor"
};

/// Print a textual representation of an instruction to the standard
/// output stream.
///
/// Examples:
///   and i0 f8 -> o4
///    or $2 $1 -> $3
///   rot $2 01 -> $2
///
/// @param[in] in instruction
void
inst_show(const struct inst in)
{
  static const char mem_str[4] = {
    '$', // Helper variable.
    'o', // Output vector element.
    'i', // Input vector element.
    ' '  // Integer constant.
  };

  info("%s%s %c%02x %c%02x -> %c%02x",
    (in.in_mo == MO_OR) ? " " : "",
    mo_str[in.in_mo],
    mem_str[in.in_op1_mem], in.in_op1_val,
    mem_str[in.in_op2_mem], in.in_op2_val,
    mem_str[in.in_res_mem], in.in_res_val);
}

/// Print a textual representation of a program to the standard output stream.
///
/// @param[in] pr program
void
prog_show(const struct prog* pr)
{
  uint8_t i;

  for (i = 0; i < pr->pr_len; i++)
    inst_show(pr->pr_iset[pr->pr_idx[i]]);
}

/// Print the user-selected configuration to the standard output stream.
///
/// @param[in] cf configuration
void
conf_show(const struct conf* cf)
{
  char res[128];
  uint16_t mos;
  uint8_t i;
  bool match;

  // Create a comma-separated list of selected mathematical operations.
  mos = cf->cf_mos;

  (void)memset(res, '\0', sizeof(res));
  for (i = 0; i < 8; i++) {
    match = false;

    if (mos & 1) {
      (void)strcat(res, mo_str[i]);
      match = true;
    }

    mos >>= 1;
    if (mos > 0 && match == true)
      (void)strcat(res, ", ");
  }

  for (i = 0; cf->cf_path[i] != NULL; i++) 
    info("shared object path: %s", cf->cf_path[i]);

  info("number of function samples: %" PRIu16, cf->cf_smp);
  info("input vector size: %" PRIu16, cf->cf_inp);
  info("output vector size: %" PRIu16, cf->cf_out);
  info("number of helper variables: %" PRIu16, cf->cf_var);
  info("constants range: 0-%" PRIu16, cf->cf_cst);
  info("mathematical operations: %s", res);
  info("matching mode: %s", cf->cf_fuz == true ? "fuzzy" : "exact");
}

/// Print a function sample.
///
/// Examples:
///   04 ee 31 -> 04 31 ee
///   38 4b 20 -> 20 38 4b
///   51 0d 11 -> 0d 11 51
///
/// @param[in] fn function
/// @param[in] si sample index
/// @param[in] cf configuration
static void
samp_show(const struct func* fn, const uint16_t si, const struct conf* cf)
{
  char res[128];
  char str[4];
  uint16_t i;

  (void)memset(res, '\0', sizeof(res));

  // Create the input vector string.
  for (i = 0; i < cf->cf_inp; i++) {
    (void)snprintf(str, sizeof(str), "%02" PRIx8 " ", fn->fn_sinp[si][i]);
    (void)strcat(res, str);
  }

  (void)strcat(res, "-> ");

  // Add the output vector string.
  for (i = 0; i < cf->cf_out; i++) {
    (void)snprintf(str, sizeof(str), "%02" PRIx8 " ", fn->fn_sout[si][i]);
    (void)strcat(res, str);
  }

  // Finally print the string.
  info(res);
}

/// Print function details.
///
/// Example:
///   sort3u/3,3
///   04 ee 31 -> 04 31 ee
///   38 4b 20 -> 20 38 4b
///   51 0d 11 -> 0d 11 51
///   f7 f7 00 -> 00 f7 f7
///   01 7f 1e -> 01 1e 7f
///
/// @param[in] fn function
/// @param[in] cf configuration
void
func_show(const struct func* fn, const struct conf* cf)
{
  uint16_t i;

  info("%s samples:", fn->fn_name);

  for (i = 0; i < cf->cf_smp; i++)
    samp_show(fn, i, cf);
}
