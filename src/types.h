// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#ifndef KY_TYPES_H
#define KY_TYPES_H

#include <stdbool.h>
#include <stdint.h>

#include "model.h"


/// Function.
struct func {
  char*   fn_name;                             ///< Name.
  bool  (*fn_comp)(uint8_t*, const uint8_t*);  ///< Computation.
  uint8_t fn_sinp[SMP_CNT_MAX][MEM_INP_MAX];   ///< Sample input vector.
  uint8_t fn_sout[SMP_CNT_MAX][MEM_OUT_MAX];   ///< Sample output vector.
};

/// Configuration.
struct conf {
  uint16_t cf_inp;               ///< Size of the input vector.
  uint16_t cf_out;               ///< Size of the output vector.
  uint16_t cf_var;               ///< Number of helper variables.
  uint16_t cf_cst;               ///< Number of numerical constants.
  uint16_t cf_len;               ///< Length of the generated program.
  uint16_t cf_mos;               ///< Bit flags for mathematical operations.
  uint16_t cf_smp;               ///< Number of generated samples per function.
  bool     cf_fuz;               ///< Fuzzy mode of output comparison.
  uint8_t  cf_pad;               ///< Padding (unused).
  char*    cf_path[FNS_CNT_MAX]; ///< Shared object paths.
};

/// Command-line option.
struct option {
  const char op_name;             ///< Name.
  bool op_arg;                    ///< Has argument?
  bool (*op_act)(struct conf* cf, ///< Action to perform.
                 const char* inp);
};

/// Instruction. (4 bytes)
struct inst {
  uint8_t in_mo      : 3; ///< Mathematical operation ID.
  uint8_t in_res_mem : 1; ///< Result memory.
  uint8_t in_res_val : 4; ///< Result value.
  uint8_t in_op1_mem : 2; ///< Operand 1 memory.
  uint8_t in_op1_val : 4; ///< Operand 1 value.
  uint8_t in_op2_mem : 2; ///< Operand 2 memory.
  uint8_t in_op2_val : 8; ///< Operand 2 value.
  uint8_t in_pad;         ///< Padding (unused).
};

/// State.
struct state {
  uint8_t st_inp[MEM_INP_MAX]; ///< Input vector.
  uint8_t st_out[MEM_OUT_MAX]; ///< Output vector.
  uint8_t st_var[MEM_VAR_MAX]; ///< Helper variables.
};

/// Program.
struct prog {
  struct inst* pr_iset;             ///< Instruction set.
  uint32_t     pr_niset;            ///< Instruction set cardinality.
  uint32_t     pr_idx[PRG_LEN_MAX]; ///< Instruction indices.
  uint8_t      pr_len;              ///< Number of assigned instructions.
  uint8_t      pr_pad[7];           ///< Padding (unused).
};

#endif
