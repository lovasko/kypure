// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdlib.h>
#include <unistd.h>
#include <dlfcn.h>
#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <inttypes.h>

#include "types.h"
#include "proto.h"
#include "model.h"


/// Dynamically load a computation from a shared library.
/// @return success/failure indication
///
/// @param[out] fn function
/// @param[in]  cf configuration
bool 
func_load(struct func* fn, const uint16_t n, const struct conf* cf)
{
  void* obj;
  uint8_t* inp;
  uint8_t* out;

  // Open the shared object file for reading.
  obj = dlopen(cf->cf_path[n], RTLD_NOW);
  if (obj == NULL) {
    info("unable to open shared object '%s': %s.", cf->cf_path[n], dlerror());
    return false;
  }

  // Load the name of the function.
  fn->fn_name = dlsym(obj, "ky_name");
  if (fn->fn_name == NULL) {
    info("unable to load symbol 'ky_name': %s", dlerror());
    return false;
  }

  // Load and verify the input vector size.
  inp = dlsym(obj, "ky_inp");
  if (inp == NULL) {
    info("unable to load symbol 'ky_inp': %s", dlerror());
    return false;
  }

  if (*inp != cf->cf_inp) {
    info("input vector does not match, expected %" PRIu16 ", actual %" PRIu16,
      *inp, cf->cf_inp);
    return false;
  }

  // Load and verify the output vector size.
  out = dlsym(obj, "ky_out");
  if (out == NULL) {
    info("unable to load symbol 'ky_out': %s", dlerror());
    return false;
  }

  if (*out != cf->cf_out) {
    info("output vector does not match, expected %" PRIu16 ", actual %" PRIu16,
      *out, cf->cf_out);
    return false;
  }

  // Load the C function representing the analysed computation.
  fn->fn_comp = dlsym(obj, "ky_func");
  if (fn->fn_comp == NULL) {
    info("unable to find symbol 'ky_func': %s.", dlerror());
    return false;
  }

  return true;
}

/// Run the computation function for a set of random points and save the
/// returned output. It is expected that the random number generator has
/// initialised properly prior to invocation of this function.
/// @return success/failure indication
///
/// @param[out] fn function
/// @param[in]  cf configuration
bool
func_samp(struct func* fn, const struct conf* cf)
{
  uint16_t i;
  uint16_t k;
  bool retb;

  // Generate random inputs and execute the computation to obtain the
  // corresponding output.
  i = 0;
  while (i < cf->cf_smp) {
    // Generate input samples based on the configuration.
    for (k = 0; k < cf->cf_inp; k++)
      fn->fn_sinp[i][k] = (uint8_t)(rand() % cf->cf_cst);

    // Run the computation based on the generated input.
    retb = fn->fn_comp(fn->fn_sout[i], fn->fn_sinp[i]);

    // The input must be accepted by the computation. Otherwise we need to
    // re-run the generation.
    if (retb == true)
      i++;
  }

  return true;
}
