// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <sys/resource.h>

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>
#include <errno.h>
#include <signal.h>

#include "model.h"
#include "proto.h"
#include "types.h"


static volatile sig_int; ///< SIGINT flag.

/// Report the system and user times of the current process.
static void
report_usage(void)
{
  struct rusage ru;
  int reti;

  reti = getrusage(RUSAGE_SELF, &ru);
  if (reti == -1) {
    info("unable to obtain run-time of the program: %s", strerror(reti));
    return;
  }

  info("user time: %lds", ru.ru_utime.tv_sec);
  info("system time: %lds", ru.ru_stime.tv_sec);
}

static void
signal_handler(int sign)
{
	if (sign == SIGINT)
	  sig_int = true;
}

/// Block the delivery of all signals, except for SIGINT.
/// @return success/failure indication
static bool
block_signals_delivery(void)
{
  sigset_t ss;
	int reti;

	(void)sigfillset(&ss);
	(void)sigdelset(&ss, SIGINT);

  reti = sigprocmask(SIG_SETMASK, &ss, NULL);
	if (reti == -1) {
	  info("unable to block signal delivery: %s", sterror(reti));
		return false;
	}

	return true;
}

/// Ensure that the appropriate response procedure is executed upon the receipt
/// of the SIGINT signal.
/// @return success/failure indication
static bool
install_signal_handler(void)
{
  int reti;
	struct sigaction sa;

  // Prepare the structure.
	(void)memset(&sa, 0, sizeof(sa));
	sa.sa_handler = signal_handler;

  // Register the handler procedure.
	reti = sigaction(SIGINT, &sa, NULL);
	if (reti == -1) {
	  info("unable to register handler for SIGINT: %s", strerror(reti));
		return false;
	}

	return true;
}

int
main(int argc, char* argv[])
{
  struct prog pr;
  struct conf cf;
  struct func fn[FNS_CNT_MAX];
  bool retb;
  uint16_t i;
  uint64_t npr;

  // Initialise the random seed.
  srand((unsigned int)time(NULL));

  // Parse the configuration from command-line options.
  retb = conf_load(&cf, argc, argv);
  if (retb == false) {
    info("unable to parse the configuration");
    return EXIT_FAILURE;
  }
  conf_show(&cf);

  // Load all user-selected shared object functions.
  for (i = 0; cf.cf_path[i] != NULL; i++) {
    retb = func_load(&fn[i], i, &cf);
    if (retb == false) {
      info("unable to load function: '%s'", cf.cf_path[i]);
      return EXIT_FAILURE;
    }

    func_samp(&fn[i], &cf);
    func_show(&fn[i], &cf);
  }

  // Generate the instruction set.
  retb = prog_iset(&pr, &cf);
  if (retb == false) {
    info("unable to populate instruction set");
    return EXIT_FAILURE;
  }

  // Traverse all programs and verify whether they satisfy the functions.
  prog_reset(&pr);
  npr = 0;
  while (true) {
    retb = prog_next(&pr, &cf);
    if (retb == false) {
      info("tried all programs: %" PRIu64, npr);
      break;
    }
    npr++;

    // Check for all functions.
    for (i = 0; cf.cf_path[i] != NULL; i++) {
      retb = prog_test_samp(&pr, &fn[i], &cf);
      if (retb == false)
        continue;

      retb = prog_test_full(&pr, &fn[i], &cf);
      if (retb == true) {
        info("program found for %s", cf.cf_path[i]);
        info("programs tried: %" PRIu64, npr);
      }
    }
  }

  report_usage();

  return EXIT_SUCCESS;
}
