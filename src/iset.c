// Copyright (c) 2018-2019 Daniel Lovasko
// All Rights Reserved
//
// Distributed under the terms of the 2-clause BSD License. The full
// license is in the file LICENSE, distributed as part of this software.

#include <stdbool.h>

#include "types.h"
#include "model.h"
#include "proto.h"


/// Check if the function is one of the user-selected functions.
/// @return decision
///
/// @param[in] in instruction
/// @param[in] cf configuration
static bool
mop_filter(const struct inst in, const struct conf* cf)
{
  return cf->cf_mos & (1 << in.in_mo);
}

/// Check if the first operand is not a numerical constant.
/// @return decision
///
/// This ensures that the memory for the first operand can be reduced to
/// only 4 bits, saving other 4 bits. We can do this, since adding two
/// numerical constants can be done in separate instructions.
///
/// @param[in] in instruction
static bool
op1_non_constant_filter(const struct inst in)
{
  return in.in_op1_mem != MEM_CST;
}

/// Check that the numerical constant in the second operand is not a number
/// that would cause no effect to the result, depending on the function used.
/// @return decision
///
/// Example: adding zero or multiplying by one.
///
/// @param[in] in instruction
static bool
idempotent_filter(const struct inst in)
{
  static const uint8_t noop_vals[6] = {
    0x00, // Integer addition.
    0x01, // Integer multiplication.
    0xff, // Logical AND.
    0x00, // Logical OR.
    0x00, // Logical exclusive OR.
    0x00, // Bit rotation.
  };

  return !(in.in_op2_mem == MEM_CST
        && in.in_mo      != MO_NND
        && in.in_mo      != MO_NOR
        && in.in_op2_val == noop_vals[in.in_mo]);
}

/// Check that the operation will not destroy the value. Given the set of
/// mathematical operations, AND and MUL can destroy the stored value by
/// applying the zero constant to it.
/// @return decision
///
/// @param[in] in instruction
static bool
destructive_filter(const struct inst in)
{
  return !(in.in_op2_mem == MEM_CST
        && in.in_op2_val == 0
        && (in.in_mo == MO_MUL || in.in_mo == MO_AND));
}

/// Check if the memory indices of the first operand are valid within the
/// selected bounds.
/// @return decision
///
/// @param[in] in instruction
/// @param[in] cf configuration
static bool
op1_indices_filter(const struct inst in, const struct conf* cf)
{
  return (in.in_op1_mem == MEM_INP && in.in_op1_val < cf->cf_inp)
      || (in.in_op1_mem == MEM_OUT && in.in_op1_val < cf->cf_out)
      || (in.in_op1_mem == MEM_VAR && in.in_op1_val < cf->cf_var);
}

/// Check if the memory indices of the second operand are valid within the
/// selected bounds.
/// @return decision
/// 
/// Compared to the first operand, the second operand is allowed to be a
/// numerical constant.
///
/// @param[in] in instruction
/// @param[in] cf configuration
static bool
op2_indices_filter(const struct inst in, const struct conf* cf)
{
  return (in.in_op2_mem == MEM_INP && in.in_op2_val < cf->cf_inp)
      || (in.in_op2_mem == MEM_OUT && in.in_op2_val < cf->cf_out)
      || (in.in_op2_mem == MEM_VAR && in.in_op2_val < cf->cf_var)
      || (in.in_op2_mem == MEM_CST && in.in_op2_val < cf->cf_cst);
}

/// Check if the memory indices of the result are valid within the selected
/// bounds.
/// @return decision
///
/// Only the output vector and the helper variables are valid memory locations.
///
/// @param[in] in instruction
/// @param[in] cf configuration
static bool
res_indices_filter(const struct inst in, const struct conf* cf)
{
  return (in.in_res_mem == MEM_OUT && in.in_res_val < cf->cf_out)
      || (in.in_res_mem == MEM_VAR && in.in_res_val < cf->cf_var);
}

/// Check whether the instruction already has a commutative equivalent in the
/// set. 
/// @return decision
///
/// Most of the functions are commutative and therefore the following equation
/// can be considered identical in terms of effects: FN X Y Z == FN Y X Z. To
/// achieve this, we introduce a rule, where the index of the memory location
/// of the second operand has to be greater or equal than the index of memory
/// location of the first operand. At the same time, we require the same
/// comparison result from the memory types. It is not important which MEM_*
/// is greater than which, just the fact that there is a well-defined ordering
/// is enough.
///
/// @param[in] in instruction
static bool
commutative_filter(const struct inst in)
{
  return (in.in_op2_mem == MEM_CST)
      ||  ((in.in_op1_mem >= in.in_op2_mem)
        && (in.in_op1_val >= in.in_op2_val));
}

/// Eliminate instructions that read from the output vector.
/// @return decision
///
/// @param[in] in instruction
static bool
out_write_only_filter(const struct inst in)
{
  return (in.in_op1_mem != MEM_OUT && in.in_op2_mem != MEM_OUT); 
}

/// Determine whether an instruction is a member of an instruction set, given
/// an instruction set definition.
/// @return decision
///
/// @param[in] in instruction
/// @param[in] cf configuration
bool
inst_iset(const struct inst in, const struct conf* cf)
{
  return mop_filter(in, cf)
      && op1_non_constant_filter(in)
      && op1_indices_filter(in, cf)
      && op2_indices_filter(in, cf)
      && res_indices_filter(in, cf)
      && idempotent_filter(in)
      && destructive_filter(in)
      && commutative_filter(in)
      && out_write_only_filter(in);
}
