#  Copyright (c) 2018-2019 Daniel Lovasko
#  All Rights Reserved
#
#  Distributed under the terms of the 2-clause BSD License. The full
#  license is in the file LICENSE, distributed as part of this software.

CC = cc
FTM = -D_ISOC99_SOURCE -D_POSIX_C_SOURCE=201901L
WARN = -Wall -Wextra -Werror -Wconversion
CFLAGS = -std=c99 $(WARN) -O3 $(FTM) -Isrc/
LDFLAGS = -ldl

all: bin/ky

# executable
bin/ky: obj/conf.o \
        obj/exec.o \
        obj/iset.o \
        obj/func.o \
        obj/info.o \
        obj/main.o \
        obj/next.o \
        obj/prog.o \
        obj/show.o \
        obj/test.o
	$(CC) obj/conf.o \
        obj/exec.o \
        obj/iset.o \
        obj/func.o \
        obj/info.o \
        obj/main.o \
        obj/next.o \
        obj/prog.o \
        obj/show.o \
        obj/test.o \
        -o bin/ky $(LDFLAGS)

# object files
obj/conf.o: src/conf.c
	$(CC) $(CFLAGS) -c src/conf.c -o obj/conf.o

obj/exec.o: src/exec.c
	$(CC) $(CFLAGS) -c src/exec.c -o obj/exec.o

obj/func.o: src/func.c
	$(CC) $(CFLAGS) -c src/func.c -o obj/func.o

obj/iset.o: src/iset.c
	$(CC) $(CFLAGS) -c src/iset.c -o obj/iset.o

obj/info.o: src/info.c
	$(CC) $(CFLAGS) -c src/info.c -o obj/info.o

obj/main.o: src/main.c
	$(CC) $(CFLAGS) -c src/main.c -o obj/main.o

obj/next.o: src/next.c
	$(CC) $(CFLAGS) -c src/next.c -o obj/next.o

obj/prog.o: src/prog.c
	$(CC) $(CFLAGS) -c src/prog.c -o obj/prog.o

obj/show.o: src/show.c
	$(CC) $(CFLAGS) -c src/show.c -o obj/show.o

obj/test.o: src/test.c
	$(CC) $(CFLAGS) -c src/test.c -o obj/test.o

clean:
	rm -f bin/ky
	rm -f obj/conf.o
	rm -f obj/exec.o
	rm -f obj/iset.o
	rm -f obj/func.o
	rm -f obj/info.o
	rm -f obj/main.o
	rm -f obj/next.o
	rm -f obj/prog.o
	rm -f obj/show.o
	rm -f obj/test.o
